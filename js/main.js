// TODO: This file is becoming incredibly untidy. Fix it!!
window.onload = init;

// Global so I can use the console.
var backingCanvas = document.getElementById('backingCanvas');
var backContext = backingCanvas.getContext('2d');
var frontCanvas = document.getElementById('frontCanvas');
var frontContext = frontCanvas.getContext('2d');
var video = document.getElementById('videoElement');
var tmpCanvas = document.getElementById('tmpCanvas');
var tmpContext = tmpCanvas.getContext('2d');

var GRAYSCALE = 1;
var CUSTOM_FILTER = 2;
var currentFilter = GRAYSCALE;

var kernelUI = document.getElementsByName('selectFilter');
for (var l = 0; l < kernelUI.length; l++) {
  kernelUI[l].onclick = updateKernelSelection;
}
function updateKernelSelection() {
  if (document.getElementById('grayscaleButton').checked) {
    currentFilter = GRAYSCALE;
  } else if (document.getElementById('customButton').checked) {
    currentFilter = CUSTOM_FILTER;
  }
};

var kernelWts = document.getElementsByName('kernelWts');
for (var l = 0; l < kernelWts.length; l++) {
  kernelWts[l].onkeyup = updateKernelKeyUp;
}
function updateKernelKeyUp(e) {
  if (e.keyCode == 13) {
    updateKernel();
  }
}

var defaultKernel = [1, 1, 1,
                     1, 0.7, -1,
                    -1, -1, -1];
var kernel = defaultKernel;

document.getElementById('applyKernel').onclick = updateKernel;
function updateKernel() {
  kernel[0] = parseFloat(document.getElementById('h00').value);
  kernel[1] = parseFloat(document.getElementById('h01').value);
  kernel[2] = parseFloat(document.getElementById('h02').value);
  
  kernel[3] = parseFloat(document.getElementById('h10').value);
  kernel[4] = parseFloat(document.getElementById('h11').value);
  kernel[5] = parseFloat(document.getElementById('h12').value);
  
  kernel[6] = parseFloat(document.getElementById('h20').value);
  kernel[7] = parseFloat(document.getElementById('h21').value);
  kernel[8] = parseFloat(document.getElementById('h22').value);
};

function init() {
  backingCanvas.width = 500;
  backingCanvas.height = 375;
  frontCanvas.width = 500;
  frontCanvas.height = 375;

  navigator.getUserMedia = navigator.getUserMedia
      || navigator.webkitGetUserMedia
      || navigator.mozGetUserMedia
      || navigator.msGetUserMedia
      || navigator.oGetUserMedia;

  if (navigator.getUserMedia) {       
    navigator.getUserMedia({video: true}, handleVideo, videoError);
  }

  video.addEventListener('play', function () {
    var $this = this;
    (function loop() {
      if (!$this.paused && !$this.ended) {
        backContext.drawImage($this, 0, 0, 500, 375);

        pixels = backContext.getImageData(0, 0, 500, 375);   
        
        switch (currentFilter) {
        case GRAYSCALE:       
          pixels = grayscale(pixels, 500, 375);
          break;
        case CUSTOM_FILTER:
          pixels = convolute(pixels, kernel);
          break;
        default:
        }
        frontContext.putImageData(pixels, 0, 0);

        setTimeout(loop, 1000 / 30); // drawing at 30 fps
      }
    })();
  }, 0);

  //draw();

  function handleVideo(stream) {
    video.src = window.URL.createObjectURL(stream);
  }

  function videoError(e) {
    alert("Your browser does not support this webcam feature.");
  }      
}

function grayscale(pixels, width, height) {
  var data = pixels.data;
  var numPixels = width*height*4;
  for (var i = 0; i < numPixels; i += 4) {
    var r = data[i];
    var g = data[i+1];
    var b = data[i+2];
    
    var v = 0.2126*r + 0.7152*g + 0.0722*b;
    data[i] = v;
    data[i+1] = v;
    data[i+2] = v;
  }
  pixels.data = data;
  return pixels;
}

function convolute(pixels, weights) {
  var side = Math.round(Math.sqrt(weights.length));
  var halfSide = Math.floor(side/2);
  var src = pixels.data;
  var sw = pixels.width;
  var sh = pixels.height;
  // Pad output by the convolution matrix
  var w = sw;
  var h = sh;
  var output = tmpContext.createImageData(w, h);
  var dst = output.data;
  for (var y = 0; y < h; y++) {
    for (var x = 0; x < w; x++) {
      var sy = y;
      var sx = x;
      var dstOff = (y*w + x)*4;
      
      var r = 0, g = 0, b = 0, a = 0;
      for (var cy = 0; cy < side; cy++) {
        for (var cx =  0; cx < side; cx++) {
          var scy = sy + cy - halfSide;
          var scx = sx + cx - halfSide;
          if (scy >= 0 && scy < sh && scx >= 0 && scx < sw) {
            var srcOff = (scy*sw+scx)*4;
            var wt = weights[cy*side+cx];
            r += src[srcOff] * wt;
            g += src[srcOff+1] * wt;
            b += src[srcOff+2] * wt;
            a += src[srcOff+3] * wt;
          }
        }
      }
      dst[dstOff] = r;
      dst[dstOff+1] = g;
      dst[dstOff+2] = b;
      dst[dstOff+3] = a + (255-a);
    }
  }
  output.data = dst;
  return output;
}